let gameURL = location.href + '#christmasfortunetelling'

export const socNetShareGameAfter = () => {

    let pTags = document.querySelectorAll('.answer-box P')

    let name = pTags[0].textContent
    let answer = pTags[1].textContent

    Array.from(document.getElementsByTagName('head')[0].querySelectorAll('META')).forEach(metaTag => {
        if (metaTag.getAttribute('property') == 'og:title') {
            metaTag.setAttribute('content', `Игра: "Святочное гадание". Ответ: ${answer}`)
            return;
        }
    })

    // VK
    document.querySelector('.soc-net-share-box__item--vk--after-game').onclick = function() {
        window.open(`http://vk.com/share.php?url=${encodeURIComponent(gameURL)}&title=${'Игра: "Святочное гадание". Ответ: '+answer}`, '', 'left=0,top=0,width=600,height=300,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')
    }
    // FA
    document.querySelector('.soc-net-share-box__item--fa--after-game').setAttribute('data-url', gameURL)
    document.querySelector('.soc-net-share-box__item--fa--after-game').setAttribute('data-hashtag', 'святочное_гадание')
    document.querySelector('.soc-net-share-box__item--fa--after-game').setAttribute('data-title', 'Игра: "Святочное гадание". Ответ: '+answer)
    // TW
    document.querySelector('.soc-net-share-box__item--tw--after-game').onclick = function() {
        window.open(`https://twitter.com/intent/tweet?url=${encodeURIComponent(gameURL)}&text=${'Игра: "Святочное гадание". Ответ: '+answer}&hashtags=${'святочное_гадание, рождество, москва24'}`, '', 'left=0,top=0,width=600,height=300,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')
    }
    // TE
    document.querySelector('.soc-net-share-box__item--te--after-game').onclick = function() {
        window.open(`https://t.me/share/url?url=${encodeURIComponent(gameURL)}&text=${'Игра: "Святочное гадание". Ответ: '+answer}`, '', 'left=0,top=0,width=600,height=300,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')
    }
}
