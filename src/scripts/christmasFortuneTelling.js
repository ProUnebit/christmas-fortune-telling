// Custom modules
import { Christmas_Fortune_Telling } from './components/GameConstr'

function DOMIsReady() {

    if (document.getElementById('christmasfortunetelling')) {

        Christmas_Fortune_Telling.gameInit()
    }
}

document.addEventListener('DOMContentLoaded', DOMIsReady);
