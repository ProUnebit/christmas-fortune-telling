import 'sharer.js'
import { socNetShareGameAfter } from '../partials/socNetShare'

class GameConstr {
    constructor(id, gameDataArr) {
        // ID
        this.id = id
        // Data
        this.gameDataArr = gameDataArr
        // Sections
        this.motherGameContainer;
        this.gameIntroBox;
        this.gameLoaderBox;
        this.gameResultBox;
        // Values
        this.playerName;
        this.previousAnswer;
    }

    gameInit() {

        this.motherGameContainer = document.querySelector(`.${this.id}`)
        this.gameIntroBox = this.motherGameContainer.querySelector(`.${this.id}__intro-box`)
        this.gameLoaderBox = this.motherGameContainer.querySelector(`.${this.id}__loader-box`)
        this.gameResultBox = this.motherGameContainer.querySelector(`.${this.id}__result-box`)

        this.gameClear()

        console.log(this.id + ' init')

        this.gameIntroPhaseActive()
    }
    gameClear() {
        this.gameIntroBox.querySelector('.input-box INPUT').value = ''
        this.playerName = ''
    }

    gameIntroPhaseActive() {
        let gameStartButton = this.gameIntroBox.querySelector('.button-box BUTTON')
        let gameInputName = this.gameIntroBox.querySelector('.input-box INPUT')

        gameInputName.oninput = () => {
            if (gameInputName.value.trim() === '') {
                gameStartButton.style.visibility = 'hidden'
            } else {
            gameStartButton.style.visibility = 'visible'
            }
        }

        gameStartButton.onclick = () => {
            this.gameSetPlayerName(gameInputName.value.trim())
            this.gameAnswerGenerator()
            this.gameLoaderPhaseActive(this.gameDataArr[this.previousAnswer])
        }
    }
    gameLoaderPhaseActive(answerString) {

        let gameIntroBoxStyles = getComputedStyle(document.querySelector(`.${this.id}__intro-box`))

        if (gameIntroBoxStyles.display !== 'none') {
            this.gameIntroBoxDisplaying()
        }

        let gameResultBoxStyles = getComputedStyle(document.querySelector(`.${this.id}__result-box`))

        if (gameResultBoxStyles.display !== 'none') {
            this.gameResultBoxDisplaying()
        }

        this.gameLoaderBoxDisplaying()

        setTimeout(() => {
            this.gameLoaderBoxDisplaying()
            this.gameResultBoxDisplaying()
            this.gameResultPhaseActive(answerString)
        }, 2725)
    }
    gameResultPhaseActive(answerStringProp) {
        let pTags = this.gameResultBox.querySelectorAll('.answer-box P')

        let name = pTags[0]
        let answer = pTags[1]

        let firstLetUppercaseName = this.playerName.charAt(0).toUpperCase() + this.playerName.slice(1) + '!'

        name.textContent = firstLetUppercaseName
        answer.textContent = answerStringProp

        let gameReloadButton = this.gameResultBox.querySelector('.button-box BUTTON')

        gameReloadButton.onclick = () => {
            this.gameAnswerGenerator()
            this.gameLoaderPhaseActive(this.gameDataArr[this.previousAnswer])
        }

        socNetShareGameAfter()
    }

    gameIntroBoxDisplaying() {
        this.gameIntroBox.classList.toggle('game-actions__displaying-off')
    }
    gameLoaderBoxDisplaying() {
        this.gameLoaderBox.classList.toggle('game-actions__displaying-on')
    }
    gameResultBoxDisplaying() {
        this.gameResultBox.classList.toggle('game-actions__displaying-on')
    }


    gameSetPlayerName(value) {
        this.playerName = value
    }
    gameAnswerGenerator() {
        let currentAnswer = this.helperRandomInteger(0, this.gameDataArr.length - 1)

        if (currentAnswer === this.previousAnswer) {
            this.gameAnswerGenerator()
            return;
        } else {
            this.previousAnswer = currentAnswer
        }
    }

    helperRandomInteger(min, max) {
        let rand = min + Math.random() * (max + 1 - min)
        rand = Math.floor(rand)
        return rand;
    }

}

export let Christmas_Fortune_Telling = new GameConstr(
    'christmas-fortune-telling',
    [
        'Автор романа обидится, если мы перескажем сюжет своими словами.',
        'Нормально делай — нормально будет!',
        'Всё будет хорошо! Или не будет. Трудно сказать…',
        'Погадайте еще раз! Учитесь получать удовольствие не только от результата, но и от процесса!',
        'Трудно сказать, будущее туманно. А через “Госуслуги” пробовали?',
        'Ответим в течение трех рабочих дней. Ожидайте.',
        'Ваш вопрос очень важен для нас. Вам ответит первый освободившийся оператор.',
        'Вы уверены, что об этом вообще сейчас стоит думать?',
        'Вам может помочь только Дед Мороз… К сожалению, он в отпуске до декабря 2019-го.',
        'Представьте, как ответила бы на этот вопрос ваша мама. Она мудрая женщина!',
        'Перестаньте задавать вопросы и начните наконец что-то делать.',
        'Спросите лучше у того, кому верите.',
        'Прежде чем гадать о будущем, научитесь радоваться настоящему.',
        'Вам выпала какая-то ерунда. Сосредоточьтесь!',
        'Ложитесь спать пораньше. Сон будет вещим!',
        'Год будет отличным! (Предсказание сбывается, только если вы гадали натощак.)',
        'Вы слишком поздно обратились к нам, все предсказания разобрали.',
        'Ваши желания исполнятся! Но в рамках текущего бюджета.',
        'Никто и ничто ждать вас не будет. Догоняйте!',
        'Ваши желания сбудутся, как только вы окончательно перехотите.',
        'Затрудняемся насчет года. Но завтра одевайтесь потеплее!'
    ]
)
